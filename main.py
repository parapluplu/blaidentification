from itertools import izip_longest
import numpy
from os import listdir
from sklearn.externals import joblib
import sys
from os.path import isfile, join
import csv
import pickle

import concurrent.futures

from de.paraplu.blaidentification.common.preprocessing.features.aggregate_mfcc import MFCCFeature
from de.paraplu.blaidentification.trainer.trainer import read_features_csv, train_model_multi_class, \
    process_file
from de.paraplu.blaidentification.user.user import perform

__author__ = 'paraplu'


def parse_csv(csv_filename):
    # Output is a dictionary with filename as key and language as value
    mapper = {}
    language_index = 0
    language_mapper = {}
    with open(csv_filename, 'rb') as csv_file:
        mapper_file = csv.reader(csv_file, delimiter=',')
        next(mapper_file, None)  # skip the headers
        for row in mapper_file:
            # row looks like: filename;language
            filename = row[0]
            language = row[1]
            lang_id = language_mapper.get(language)
            if lang_id is None:
                lang_id = language_index
                language_mapper[language] = lang_id
                language_index += 1
            mapper[filename] = lang_id
    return mapper, {y: x for x, y in language_mapper.iteritems()}


def process_files(_files, _features, _mapper):
    try:
        _vectors = []
        for _file in _files:
            if _file is not None:
                _vectors.append(process_file(_file, _features, _mapper))
        return _vectors
    except AttributeError, e:
        print e


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)


def train(csv_file, lang_csv, folder, threads):
    # essentia.log.infoActive = False  # activate the info level
    mapper, language_mapper = parse_csv(lang_csv)
    only_files = [join(folder, f) for f in listdir(folder) if isfile(join(folder, f)) if '.csv' not in f]
    features = [MFCCFeature()]
    mexecutor = concurrent.futures.ProcessPoolExecutor(threads)
    futures = [mexecutor.submit(process_files, files, features, mapper)
               for files in grouper(only_files, 10)]
    concurrent.futures.wait(futures)
    vectors = []
    for future in futures:
        vectors.extend(future.result())
    numpy.savetxt(csv_file, vectors, delimiter=';')
    vectors = read_features_csv(csv_file)
    classifier, normalizer, imputer = train_model_multi_class(vectors)
    return classifier, normalizer, language_mapper, imputer


if __name__ == '__main__':
    csv_file = sys.argv[1]
    lang_csv = sys.argv[2]
    folder = sys.argv[3]
    pickle_path = sys.argv[4]
    threads = int(sys.argv[5])
    trainingdata = sys.argv[6]
    classifier, normalizer, language_mapper, imputer = train(csv_file, lang_csv, folder, threads)
    pickle.dump(language_mapper, open(join(pickle_path, "language_mapper.pickle"), "wb"))
    pickle.dump(normalizer, open(join(pickle_path, "normalizer.pickle"), "wb"))
    pickle.dump(imputer, open(join(pickle_path, "imputer.pickle"), "wb"))
    pickle.dump(language_mapper, open(join(pickle_path, "language_mapper.pickle"), "wb"))
    joblib.dump(classifier, join(pickle_path, 'model/classifier'))
    perform(pickle_path, trainingdata, threads)