import numpy
import os
from sklearn import cross_validation, svm
from sklearn.externals import joblib
import pickle
import datetime
from sklearn.multiclass import OneVsRestClassifier
from sklearn import preprocessing

from de.paraplu.blaidentification.common.preprocessing.preprocess import preprocess

__author__ = 'paraplu'


def calculate(classification_model, test_vectors):
    # TODO calculate a weight based on the test results
    return 0


# def train_model(vectors, test_vectors):
# models = Models()
# for language in vectors.keys():
#     classifier = None  # TODO instantiate classifier here
#     classification_model = classifier.train(vectors[language])
#     weight = calculate(classification_model, test_vectors[language])
#     models.add(language, classification_model, weight)
# return models


def train_model_multi_class(_vectors):
    total_length = len(_vectors[0, :])
    print total_length
    x = _vectors[:, range(total_length - 1)]
    imputer = preprocessing.Imputer()
    x = imputer.fit_transform(x)
    normalizer = preprocessing.MinMaxScaler()
    x = normalizer.fit_transform(x)
    y = numpy.ravel(_vectors[:, total_length - 1]).astype(int)
    classifier = OneVsRestClassifier(svm.LinearSVC(C=1.0, dual=False), n_jobs=-1)
    scores = cross_validation.cross_val_score(
        classifier, x, y, cv=2)
    classifier.fit(x, y)
    print scores
    print "Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2)
    return classifier, normalizer, imputer


def pickle_model(_model, path):
    joblib.dump(_model.classifier, path + '/classifier')
    with open(path + '/language_mapping', 'wb') as f:
        pickle.dump(_model.language_mapping, f)


counter = 0


def process_file(_file, features, mapper):
    try:
        global counter
        if counter % 100 == 0:
            print datetime.datetime.now(), "Processed ", counter, " in the thread"
        file_name = os.path.basename(_file)
        language = mapper[file_name]
        if language is None:
            raise ValueError(file_name + " is not mentioned in mapper file")
        vector = preprocess(_file, features, language)
        counter += 1
        return vector
    except RuntimeError, e:
        print "error while processing", e


def read_features_csv(_csv):
    _vectors = numpy.loadtxt(_csv, delimiter=';')
    return _vectors