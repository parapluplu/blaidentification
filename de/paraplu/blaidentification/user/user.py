from itertools import izip_longest
import numpy
import pickle
from sklearn.externals import joblib
from os.path import join, isfile
from os import listdir, path

import essentia
import concurrent.futures

from de.paraplu.blaidentification.common.preprocessing.features.aggregate_mfcc import AggregateMFCCFeature

__author__ = 'paraplu'


def read_wav(file):
    loader = essentia.standard.MonoLoader(filename=file)
    audio = loader()
    return audio


def p(file, features):
    v = numpy.array([])
    for feature in features:
        feature_vector = feature.extract(file)
        v = feature_vector
    return v, file


def predict(features, f, language_model, normalizer, imputer, classifier):
    v = imputer.transform(features)
    v = normalizer.transform(v)
    probs = classifier.decision_function(v)[0]
    top3 = probs.argsort()[-3:][::-1]
    vals = []
    for i in range(len(top3)):
        language = language_model[top3[i]]
        vals.append(f + "," + language + "," + str(i + 1))
    return vals


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)


def perform(base, trainingdata, threads):
    features = [AggregateMFCCFeature()]
    only_files = [join(trainingdata, f) for f in listdir(trainingdata) if isfile(join(trainingdata, f)) if
                  '.csv' not in f]
    mexecutor = concurrent.futures.ProcessPoolExecutor(threads)
    futures = [mexecutor.submit(p, file, features)
               for file in only_files]
    concurrent.futures.wait(futures)
    file_vectors = {}
    for future in futures:
        features, f = future.result()
        file_vectors[f] = features

    pickle.dump(file_vectors, open(join(base, "trpre.pickle"), "wb"))
    print "Saved features"
    normalizer = pickle.load(open(join(base, "normalizer.pickle"), "rb"))
    imputer = pickle.load(open(join(base, "imputer.pickle"), "rb"))
    file_vectors = pickle.load(open(join(base, "trpre.pickle"), "rb"))
    language_model = pickle.load(open(join(base, "language_mapper.pickle"), "rb"))
    classifier = joblib.load(join(base, "model/classifier"))
    output = open(join(base, "result.csv"), "w")
    mexecutor = concurrent.futures.ProcessPoolExecutor(threads)
    futures = [mexecutor.submit(predict, features, path.basename(f), language_model, normalizer, imputer, classifier)
               for f, features
               in file_vectors.iteritems()]
    concurrent.futures.wait(futures)
    print "write result"
    for future in futures:
        output.write('\n'.join(future.result()) + '\n')

    output.close()
