from pylab import show

from essentia.standard import *
import numpy

from de.paraplu.blaidentification.common.preprocessing.features.wav_feature_extractor import WAVFeatureExtractor

__author__ = 'paraplu'

LOW_LEVEL_MFCC = 'lowlevel.mfcc'
LOW_LEVEL_MFCC_BANDS = 'lowlevel.mfcc_bands'
frame_size = 2048
hop_size = 1024
sample_rate = 44100
bands = 40
coeffs = 13

class MFCCFeature():
    def __init__(self):
        pass

    def extract(self, _file, language):
        loader = MonoLoader(filename=_file)
        _audio = loader()
        spectrum = Spectrum(size=frame_size)
        sc = SpectralContrast(frameSize=frame_size, sampleRate=sample_rate)
        energy = Energy()
        gfcc = GFCC(sampleRate=sample_rate, numberBands=bands, numberCoefficients=coeffs)
        mfcc = MFCC(inputSize=frame_size / 2 + 1, sampleRate=sample_rate, numberBands=bands, numberCoefficients=coeffs)
        lpc = LPC(sampleRate=sample_rate)
        w = Windowing(type='hann')
        pool = essentia.Pool()
        vecs = []
        for frame in FrameGenerator(_audio, frameSize=frame_size, hopSize=hop_size):
            if not essentia.isSilent(frame):
                v = []
                spec = spectrum(w(frame))
                mfcc_bands, mfcc_coeffs = mfcc(spec)
                gfcc_bands, gfcc_coeffs = gfcc(spec)
                energy_val = energy(spec)
                lpc_coeff, reflection_coeff = lpc(frame)
                # print "mfcc_bands", mfcc_bands
                spec_contr, o = sc(spec)
                v.extend(mfcc_coeffs)
                v.extend(gfcc_coeffs)
                v.append(energy_val)
                v.extend(spec_contr)
                v.extend(lpc_coeff)
                v.extend(reflection_coeff)
                v.append(language)
                vecs.append(v)
        return numpy.array(vecs)


if __name__ == '__main__':
    e = MFCCFeature()
    for file in ("/home/paraplu/trainingdata/004ll12esqk.mp3", "/home/paraplu/trainingdata/00c3d3fggm3.mp3"):
        e.extract(file)
    show()  # unnecessary if you started "ipython --pylab"