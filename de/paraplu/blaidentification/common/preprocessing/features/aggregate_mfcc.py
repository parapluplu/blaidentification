import numpy

from essentia.standard import PoolAggregator

from de.paraplu.blaidentification.common.preprocessing.features.mfcc import MFCCFeature

__author__ = 'paraplu'


class AggregateMFCCFeature(MFCCFeature):
    def __init__(self):
        MFCCFeature.__init__(self)

    def extract(self, wav):
        try:
            pool = MFCCFeature.extract(self, wav)
            self.calc_deltas(pool, pool['lowlevel.mfcc'], 'mfcc')
            self.calc_deltas(pool, pool['lowlevel.gfcc'], 'gfcc')
            self.calc_deltas(pool, pool['lowlevel.lpc_coeff'], 'lpc_coeff')
            self.calc_deltas(pool, pool['lowlevel.lpc_reflection_coeff'], 'lpc_reflection_coeff')
            stats = ['median', 'mean', 'var', 'min', 'max', 'skew', 'kurt', 'dmean', 'dvar', 'dmean2', 'dvar2']
            aggrPool = PoolAggregator(defaultStats=stats)(pool)
            keys = ['lowlevel.mfcc', 'mfcc.deltas', 'mfcc.deltas_square', 'lowlevel.gfcc', 'gfcc.deltas',
                    'gfcc.deltas_square', 'energy', 'spec_contr', 'lowlevel.lpc_reflection_coeff', 'lowlevel.lpc_coeff',
                    'lpc_coeff.deltas', 'lpc_coeff.deltas_square', 'lpc_reflection_coeff.deltas',
                    'lpc_reflection_coeff.deltas_square']
            r_keys = [key + "." + stat for stat in stats for key in keys]
            vector = []
            for key in r_keys:
                data = aggrPool[key]
                if isinstance(data, float):
                    vector.append(data)
                else:
                    vector.extend(data)
            return numpy.array(vector)
        except TypeError, e:
            print "aggr", e

    def calc_deltas(self, pool, v, prefix):
        for x in range(1, len(v) - 1):
            deltas = []
            deltas_square = []
            for y in range(len(v[x])):
                c = v[x][y]
                c_before = v[x - 1][y]
                c_after = v[x + 1][y]
                d = (c_after - c_before) / 2
                d_square = (c_after - (2 * c) + c_before) / 4
                deltas.append(d)
                deltas_square.append(d_square)
            pool.add(prefix + '.deltas', deltas)
            pool.add(prefix + '.deltas_square', deltas_square)


if __name__ == '__main__':
    e = AggregateMFCCFeature()
    for file in ("/home/paraplu/trainingdata/004ll12esqk.mp3", "/home/paraplu/trainingdata/00c3d3fggm3.mp3"):
        print len(e.extract(file))
