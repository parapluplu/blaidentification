import numpy

import essentia

__author__ = 'paraplu'


def read_wav(file):
    loader = essentia.standard.MonoLoader(filename=file)
    audio = loader()
    return audio


def preprocess(file, features, language):
    v = numpy.array([])
    for feature in features:
        feature_vector = feature.extract(file, language)
        v = feature_vector
    return v
