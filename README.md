# blaidentification

For documentation read the following [google docs document](https://docs.google.com/document/d/1y666cWngl8r_H2YQvSZRLzOB1_7Ne-UeQo8E0n7GV0w/edit?usp=sharing).

Software Stack:
- Python
- Some Audio Extraction Library (Yafee, Essentia?)
- Some Classification library (Scikit?)
